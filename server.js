//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.use(express.static(__dirname + "/build/default"));
app.listen(port);

console.log('Iniciando aplicacion polymer desde node: ' + port);

// Se define la peticion app get y redirige a directorio raiz /index.html
app.get('/', function (req, res) {
  // Se sustituye
  //res.sendfile(path.join(__dirname, "index.html"));
  res.sendfile("index.html", { root: '.' })
}
);
